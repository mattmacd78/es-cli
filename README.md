#### es-cli

Custom shell functions providing an easy-to-use `ElasticSearch client` / `DSL` / `CLI`

#### Examples :


###### ES URL and defaults

Specify ES URL and load custom functions library

    # ES=10.9.3.231:9200 source es-cli.sh


`ES` URL defaults to *127.0.0.1:9200*

Therefore

    # ES=127.0.0.1:9200 source es-cli.sh


is the same as

    # source es-cli.sh

###### Raw `elasticsearch` output

By default, `cURL` output is unprocessed and will be displayed as sent by `elasticsearch`

    # GET _all
    {".kibana_1":{"aliases":{".kibana":{}},"mappings":{"doc":{"dynam

  `json` provides an "alias" *(actually a function)* to `jq` providing searchable colorized & pretty-print output

    # GET _all | json
    {
      ".kibana_1": {
        "aliases": {
          ".kibana": {}
        },
        "mappings": {
          "doc": {
            "dynamic": "strict",
            "properties": {
              "apm-telemetry": {

###### Query output using `json`

Retrieving, searching, filtering, transforming structured elasticsearch output is done using json

    # GET _cluster/health | json .status
    "yellow"

`json` is merely an alias function to the `jq` language through its `CLI` and `options`

    # GET _cluster/health | json .status -cr
    yellow

It obeys the same constraints and usage

Piping output requires a filter, the most basic being dot ( . )

The filter can be omitted when `jq` / `json` is the last command in the chain

    # GET _cluster/health | json
    {
      "cluster_name": "elasticsearch",
      "status": "yellow",
      "timed_out": false,
      "number_of_nodes": 1,
      "number_of_data_nodes": 1,
      "active_primary_shards": 25,
      "active_shards": 25,
      "relocating_shards": 0,
      "initializing_shards": 0,
      "unassigned_shards": 8,
      "delayed_unassigned_shards": 0,
      "number_of_pending_tasks": 0,
      "number_of_in_flight_fetch": 0,
      "task_max_waiting_in_queue_millis": 0,
      "active_shards_percent_as_number": 75.75757575757575
    }

The filter **must** however be specified **when** piping output of `jq` / `json`

    # GET _cluster/health | json . | less
    {
      "cluster_name": "elasticsearch",
      "status": "yellow",
      "timed_out": false,
      "number_of_nodes": 1,
      "number_of_data_nodes": 1,
      "active_primary_shards": 25,
      "active_shards": 25,
      "relocating_shards": 0,
      "initializing_shards": 0,
      "unassigned_shards": 8,
      "delayed_unassigned_shards": 0,
      "number_of_pending_tasks": 0,
      "number_of_in_flight_fetch": 0,
      "task_max_waiting_in_queue_millis": 0,
      "active_shards_percent_as_number": 75.75757575757575
    }

Not doing so returns an error and displays `jq` / `json` usage

    # GET _cluster/health | json | less
    jq - commandline JSON processor [version 1.5]
    Usage: /usr/bin/jq [options] <jq filter> [file...]

            jq is a tool for processing JSON inputs, applying the
            given filter to its JSON text inputs and producing the
            filter's results as JSON on standard output.
            The simplest filter is ., which is the identity filter,
            copying jq's input to its output unmodified (except for
            formatting).
            For more advanced filters see the jq(1) manpage ("man jq")
            and/or https://stedolan.github.io/jq

            Some of the options include:
             -c             compact instead of pretty-printed output;
             -n             use `null` as the single input value;
             -e             set the exit status code based on the output;
             -s             read (slurp) all inputs into an array; apply filter to it;
             -r             output raw strings, not JSON texts;
             -R             read raw strings, not JSON texts;
             -C             colorize JSON;
             -M             monochrome (don't colorize JSON);
             -S             sort keys of objects on output;
             --tab  use tabs for indentation;
             --arg a v      set variable $a to value <v>;
             --argjson a v  set variable $a to JSON value <v>;
             --slurpfile a f        set variable $a to an array of JSON texts read from <f>;
            See the manpage for more options.
    (END)(23) Failed writing body

###### JSON output

Most `elasticsearch` endpoints return compact `JSON`, some return it pretty-printed by default *(but not colorized though)*

    # GET
    {
      "name" : "Rvi0iyD",
      "cluster_name" : "elasticsearch",
      "cluster_uuid" : "J9O5GY27S3GrPsFkb1MtPg",
      "version" : {
        "number" : "6.7.2",
        "build_flavor" : "default",
        "build_type" : "rpm",
        "build_hash" : "56c6e48",
        "build_date" : "2019-04-29T09:05:50.290371Z",
        "build_snapshot" : false,
        "lucene_version" : "7.7.0",
        "minimum_wire_compatibility_version" : "5.6.0",
        "minimum_index_compatibility_version" : "5.0.0"
      },
      "tagline" : "You Know, for Search"
    }

Other endpoints return `text`

    # GET _cat/count
    1567237280 07:41:20 3586709

###### Requesting JSON output from elasticsearch

Requesting JSON output from `elasticsearch` is done like so

    # GET _cat/count?format=json
    [{"epoch":"1567237287","timestamp":"07:41:27","count":"3586999"}]

###### Requesting pretty-printed JSON output from elasticsearch

Requesting pretty-printed JSON output from elasticsearch is done like so

    # GET "_cat/count?format=json&pretty"
    [
      {
        "epoch" : "1567237307",
        "timestamp" : "07:41:47",
        "count" : "3587895"
      }
    ]

###### Loop utility

`loop` wraps your command in a loop

    # loop GET _cluster/health
    {"cluster_name":"elasticsearch","status":"yellow","timed_out":false,"number_of_nodes":1,"number_of_data_nodes":1,"}
    {"cluster_name":"elasticsearch","status":"yellow","timed_out":false,"number_of_nodes":1,"number_of_data_nodes":1,"}

###### Loop interval as INT

`DELAY` defaults to `1s` but is can be modified like so

    # DELAY=3 loop GET _cluster/health

###### Loop interval as FLOAT

Floating point `DELAY` *(in seconds)* is specified like so

    # DELAY=0.333 loop GET _cluster/health

###### Useful commands

    # GET "_tasks?detailed=true&actions=*"
    {"nodes":{"Rvi0iyDsTjOXXfZklwwO6g":{"name":"Rvi0iyD","transport_address":"192.168.0.7:9300","host":"192.168.0.7","}

    # GET "_tasks?detailed=true&actions=*bulk*"
    {"nodes":{}}

    # GET "_tasks?detailed=true&actions=*index*"
    {"nodes":{}}

    # GET "_tasks?detailed=true&actions=*reindex*"
    {"nodes":{}}

    # GET "_tasks?detailed=true&actions=*search*"
    {"nodes":{}}

    # GET "_tasks?detailed=true&actions=*search*"
    {"nodes":{}}

#!/bin/bash

test -z $ES && ES=127.0.0.1:9200

PS1="# "

json() { /usr/bin/jq ${@} ; }
es()   { /usr/bin/curl --buffer --silent --header 'Content-Type: application/json' ${@} ; }

GET()    { es -XGET    ${ES}/${@} ; }
PUT()    { es -XPUT    ${ES}/${@} ; }
POST()   { es -XPOST   ${ES}/${@} ; }
HEAD()   { es --head   ${ES}/${@} ; }
DELETE() { es -XDELETE ${ES}/${@} ; }

loop() { while true ; do ${*} ; sleep ${DELAY:-1} ; done }
